// Arduino music box
#define DEFAULT_DELAY 200
#define CONTROL_DELAY 300
#define BAUD_RATE 115200
// ---------------------------------------
// File Player
// ---------------------------------------
#define DEFAULT_VOLUME 20
#define MONO_MODE 1
#define FILENAME_LENGTH 13
#define MIN_TRACK_NO 1
#define MAX_VOLUME 2
#define MIN_VOLUME 254
#define STEP_VOLUME 2

#include <SPI.h>
#include <SdFat.h>
#include <FreeStack.h>
#include <SFEMP3Shield.h>

// Below is not needed if interrupt driven. Safe to remove if not using.
#if defined(USE_MP3_REFILL_MEANS) && USE_MP3_REFILL_MEANS == USE_MP3_Timer1
  #include <TimerOne.h>
#elif defined(USE_MP3_REFILL_MEANS) && USE_MP3_REFILL_MEANS == USE_MP3_SimpleTimer
  #include <SimpleTimer.h>
#endif

SdFat sd;
SFEMP3Shield MP3player;

int8_t current_track;
int8_t num_of_tracks;

// ---------------------------------------
// Tilting Sensor
// ---------------------------------------
#include<Wire.h>

#define ANGLE_HIGH 340
#define ANGLE_LOW 20
#define ANGLE_MID 180

const int MPU_addr=0x68;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

int minVal=265;
int maxVal=402;

double x;
double y;
double z;
 
void setup(){
  // ---------------------------------------
  // File Player
  // ---------------------------------------
  uint8_t result; //result code from some function as to be tested at later time.
   
  Serial.begin(BAUD_RATE);
  randomSeed(analogRead(0));
  Serial.print(F("F_CPU = "));
  Serial.println(F_CPU);
  Serial.print(F("Free RAM = ")); // available in Version 1.0 F() bases the string to into Flash, to use less SRAM.
  Serial.print(FreeStack(), DEC);  // FreeRam() is provided by SdFatUtil.h
  Serial.println(F(" Should be a base line of 1017, on ATmega328 when using INTx"));
  
  //Initialize the SdCard.
  if(!sd.begin(SD_SEL, SPI_FULL_SPEED)) sd.initErrorHalt();
  // depending upon your SdCard environment, SPI_HAVE_SPEED may work better.
  if(!sd.chdir("/")) sd.errorHalt("sd.chdir");
  
  //Initialize the MP3 Player Shield
  result = MP3player.begin();
  //check result, see readme for error codes.
  if(result != 0) {
    Serial.print(F("Error code: "));
    Serial.print(result);
    Serial.println(F(" when trying to start MP3 player"));
    if( result == 6 ) {
      Serial.println(F("Warning: patch file not found, skipping.")); // can be removed for space, if needed.
      Serial.println(F("Use the \"d\" command to verify SdCard can be read")); // can be removed for space, if needed.
    }
  }
#if (0)
  // Typically not used by most shields, hence commented out.
  Serial.println(F("Applying ADMixer patch."));
  if(MP3player.ADMixerLoad("admxster.053") == 0) {
    Serial.println(F("Setting ADMixer Volume."));
    MP3player.ADMixerVol(-3);
  }
#endif

  num_of_tracks = count_tracks();
  
  MP3player.setVolume(DEFAULT_VOLUME, DEFAULT_VOLUME); 
  MP3player.setMonoMode(MONO_MODE);
  if (MP3player.getMonoMode() == MONO_MODE) {
    Serial.println("MP3 Player in mono mode!");
  } else {
    Serial.println("MP3 Player in stereo mode!");
  }
  
  // ---------------------------------------
  // Tilting Sensor
  // ---------------------------------------
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
}

void loop(){
  
  // ---------------------------------------
  // File Player
  // ---------------------------------------
  // Below is only needed if not interrupt driven. Safe to remove if not using.
#if defined(USE_MP3_REFILL_MEANS) \
    && ( (USE_MP3_REFILL_MEANS == USE_MP3_SimpleTimer) \
    ||   (USE_MP3_REFILL_MEANS == USE_MP3_Polled)      )

  MP3player.available();
#endif

  // ---------------------------------------
  // Tilting Sensor
  // ---------------------------------------
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);
  AcX=Wire.read()<<8|Wire.read();
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
  
  int xAng = map(AcX,minVal,maxVal,-90,90);
  int yAng = map(AcY,minVal,maxVal,-90,90);
  int zAng = map(AcZ,minVal,maxVal,-90,90);

  x= RAD_TO_DEG * (atan2(-yAng, -zAng)+PI);
  y= RAD_TO_DEG * (atan2(-xAng, -zAng)+PI);
  z= RAD_TO_DEG * (atan2(-yAng, -xAng)+PI);

  if(y > ANGLE_HIGH || y < ANGLE_LOW){
    if (x > ANGLE_MID && x < ANGLE_HIGH) {
      changeVolume(true);
    } else if (x  > ANGLE_LOW && x < ANGLE_MID) {
      changeVolume(false);
    } 
  }
   
  if(x > ANGLE_HIGH || x < ANGLE_LOW){
    if (y > ANGLE_MID && y < ANGLE_HIGH) {
      //nextTrack();
      randomTrack();
      delay(CONTROL_DELAY);
    } else if (y  > ANGLE_LOW && y < ANGLE_MID) {
      playMusic();
      delay(CONTROL_DELAY);
    } 
  }

  // Play next track when current track has ended
  if (MP3player.getState() == ready) {
    randomTrack();
  }
  
  delay(DEFAULT_DELAY);
}

int count_tracks(){
  SdFile file;
  char filename[FILENAME_LENGTH];
  int fileCount = 0;
  
  sd.chdir("/",true);
  while (file.openNext(sd.vwd(), O_READ)) {
    if(!file.isHidden()){
      file.getName(filename, sizeof(filename));
      if ( isFnMusic(filename) ) {
        fileCount++;
      }
    }
    file.close();
  }
  Serial.print("Num of Files: ");
  Serial.println(fileCount);
  return fileCount;
}

int getRandomTrack(){
  int random_track;
  do {
    random_track = random(MIN_TRACK_NO, num_of_tracks);
  } while (random_track == current_track);
  
  return random_track;
}

void randomTrack(){
  current_track = getRandomTrack();
 
  char trackname[] = "track001.mp3";
  sprintf(trackname, "track%03d.mp3", current_track);
  Serial.print("Playing ");
  Serial.println(trackname);
  
  MP3player.stopTrack();
  MP3player.playTrack(current_track);
}

void changeVolume(bool up){
  union twobyte mp3_vol; // create key_command existing variable that can be both word and double byte of left and right.
  mp3_vol.word = MP3player.getVolume(); // returns a double uint8_t of Left and Right packed into int16_t

  if(up) { // note dB is negative
    if(mp3_vol.byte[1] <= MAX_VOLUME) { // range check
      mp3_vol.byte[1] = MAX_VOLUME;
    } else {
      mp3_vol.byte[1] -= STEP_VOLUME;
    }
  } else {
    // assume equal balance and use byte[1] for math
    if(mp3_vol.byte[1] >= MIN_VOLUME) { // range check
      mp3_vol.byte[1] = MIN_VOLUME;
    } else {
      mp3_vol.byte[1] += STEP_VOLUME; // keep it simpler with whole dB's
    }
  }
   
  // push byte[1] into both left and right assuming equal balance.
  MP3player.setVolume(mp3_vol.byte[1], mp3_vol.byte[1]); // commit new volume
  Serial.print(F("Volume changed to -"));
  Serial.print(mp3_vol.byte[1]>>1, 1);
  Serial.println(F("[dB]"));
}

void playMusic(){
  uint8_t result; 
  if( MP3player.getState() == playback) {
    MP3player.pauseMusic();
    Serial.println(F("Pausing"));
  } else if( MP3player.getState() == paused_playback) {
    MP3player.resumeMusic();
    Serial.println(F("Resuming"));
  } else {
    randomTrack();
  }
}
